var Lottery = {};

Lottery.swiper = function(){
    var swiper = new Swiper('.swiper-container', {
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    });
}

Lottery.init = function() {
    this.swiper();
}

$(document).ready(function() {
    Lottery.init();
});